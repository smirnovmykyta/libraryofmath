package com.gmail.mykyta.smirnov.task2;

public class Main {
    public static void main(String[] args) {

        int distanceTowards = DistanceBetweenTwoCars.distanceIfTowardsEachOther(100,60, 65, 180);
        int distanceFrom = DistanceBetweenTwoCars.distanceIfFromEachOther(100, 60, 65, 180);

        System.out.println("Расстояние между двумя машинами, если они изначально едут друг на друга: " + distanceTowards + " метров");
        System.out.println("Расстояние между двумя машинами, если они изначально едут друг от друга: " + distanceFrom + " метров");
    }
}
