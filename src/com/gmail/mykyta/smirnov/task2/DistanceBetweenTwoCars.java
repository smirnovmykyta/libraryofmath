package com.gmail.mykyta.smirnov.task2;

public class DistanceBetweenTwoCars {

    public static int distanceIfTowardsEachOther(int distance, int speedFirstCar, int speedSecondCar, int time){

        int distanceAfterTime = Math.abs(distance - (speedFirstCar + speedSecondCar) * time);

        return distanceAfterTime;

    }

    public static int distanceIfFromEachOther(int distance, int speedFirstCar, int speedSecondCar, int time){

        int distanceAfterTime = Math.abs(distance + (speedFirstCar + speedSecondCar) * time);

        return distanceAfterTime;

    }
}
