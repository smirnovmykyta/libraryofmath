package com.gmail.mykyta.smirnov.task1;

public class ProjectileFlightDistance {
    public static String projectileFlightDistanceWithRadians(int speed, double alphaInRadians) {

        double distance = ((speed * speed) * Math.sin(2 * alphaInRadians)) / 9.8;
        String formatDistance = String.format("%.2f", distance);

        return formatDistance;
    }

    public static String projectFlightDistanceWithDegrees(int speed, int alphaInDegrees) {

        double distance = ((speed * speed) * Math.sin(Math.toRadians(2 * alphaInDegrees))) / 9.8;
        String formatDistance = String.format("%.2f", distance);

        return formatDistance;
    }
}
