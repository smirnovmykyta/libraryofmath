package com.gmail.mykyta.smirnov.task4;

public class Main {
    public static void main(String[] args) {

        int x = 10;
        double z = ((6 * Math.log(Math.sqrt(Math.pow(Math.E, x + 1) + (2 * Math.pow(Math.E, x)) * Math.cos(x)))) / (Math.log(x - Math.pow(Math.E, x + 1) * Math.sin(x)))) + Math.abs(Math.cos(x) / (Math.pow(Math.E, Math.sin(x))));
        String formatZ = String.format("%.2f", z);

        System.out.println("Значение выражения при х = 10: " + formatZ);
    }

}
