package com.gmail.mykyta.smirnov.task3;

public class PointInArea {
    public static void pointInArea(double x, double y){
        if((x >= 0) && (y >= 1.5 * x - 1) && (y <= x) || (x <= 0) && (y >= -1.5 * x - 1) && (y <= -x)){
            System.out.println("Точка лежит в заштрихованой области.");
        }else {
            System.out.println("Точка не лежит в заштрихованой области.");
        }
    }
}
