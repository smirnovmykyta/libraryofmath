package com.gmail.mykyta.smirnov.task1;

public class Main {
    public static void main(String[] args) {

        String distanceWithRadians = ProjectileFlightDistance.projectileFlightDistanceWithRadians(125, 1.0472);
        String distanceWithDegrees = ProjectileFlightDistance.projectFlightDistanceWithDegrees(125, 60);



        System.out.println("Расчет расстояния полета снаряда с использованием угла в радианах : " + distanceWithRadians + " метров");
        System.out.println("Расчет расстояния полета снаряда с использованием угла в градусах : " + distanceWithDegrees + " метров");
    }
}
